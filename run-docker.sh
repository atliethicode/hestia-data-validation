#!/bin/sh
docker build -t hestia-data-validation:latest .
docker run --rm \
  --name hestia-data-validation \
  --env-file .env \
  -v ${PWD}/data:/app/data \
  hestia-data-validation:latest
