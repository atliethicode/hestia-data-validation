#!/usr/bin/env python3
import sys
import json

from hestia_earth.validation import validate


def main(args):
    from_file = args[0]
    with open(from_file, 'r') as f:
        data = f.read()

    data = validate(json.loads(data)['nodes'])

    with open(from_file, 'w') as f:
        f.write(json.dumps(data, indent=2, ensure_ascii=False))


if __name__ == "__main__":
    main(sys.argv[1:])
