#!/bin/sh
docker build -t hestia-data-validation:test -f tests/Dockerfile .
docker run --rm \
  --env-file .env \
  -v ${PWD}/coverage:/app/coverage \
  hestia-data-validation:test
