FROM nikolaik/python-nodejs:python3.7-nodejs12

WORKDIR /app

COPY package.json .
COPY package-lock.json .
RUN npm config set @hestia-earth:registry https://gitlab.com/api/v4/projects/19965626/packages/npm/
RUN npm install

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY hestia_earth ./hestia_earth
COPY gulpfile.js .
COPY *.py ./

CMD npm start
