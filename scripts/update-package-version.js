const { readFileSync, writeFileSync } = require('fs');
const { resolve, join } = require('path');

const ROOT = resolve(join(__dirname, '../'));
const version = require(join(ROOT, 'package.json')).version;

const VERSION_PATH = resolve(join(ROOT, 'hestia_earth', 'validation', 'version.py'));
let content = readFileSync(VERSION_PATH, 'UTF-8');
content = content.replace(/VERSION\s=\s\'[\d\-a-z\.]+\'/, `VERSION = '${version}'`);
writeFileSync(VERSION_PATH, content, 'UTF-8');
