const { watch } = require('gulp');
const { spawn } = require('child_process');

const watchJsonFiles = () => {
  const files = [
    `/app/data/**/*.json`,
    `/app/data/**/*.hestia`
  ];
  watch(files).on('add', file => {
    const now = new Date();
    console.log('validateData', file);
    const process = spawn('python', ['validateData.py', file]);
    process.stdout.on('data', data => console.log(data.toString()));
    process.stderr.on('data', data => {
      console.error('Error when validating file', file, data.toString());
    });
    process.on('close', code => {
      if (code === 0) {
        console.log('Finished validating file in', new Date().getTime() - now.getTime(), 'ms');
      }
    });
  });
  console.log('Watching', files);
};

exports.default = watchJsonFiles;
