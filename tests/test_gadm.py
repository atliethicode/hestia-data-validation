import unittest

from hestia_earth.validation.gadm import id_to_level


class TestGadm(unittest.TestCase):
    def test_id_to_level(self):
        self.assertEqual(id_to_level('GADM-ITA'), 0)
        self.assertEqual(id_to_level('GADM-ITA.16_1'), 1)
        self.assertEqual(id_to_level('GADM-ITA.16.10_1'), 2)
        self.assertEqual(id_to_level('GADM-ITA.16.10.3_1'), 3)
        self.assertEqual(id_to_level('GADM-RWA.5.3.10.4_1'), 4)
        self.assertEqual(id_to_level('GADM-RWA.5.3.10.4.3_1'), 5)


if __name__ == '__main__':
    unittest.main()
