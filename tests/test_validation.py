import unittest
from unittest.mock import patch

from hestia_earth.validation import validate


class TestValidation(unittest.TestCase):
    @patch('hestia_earth.validation.validate_node')
    def test_validate_call_validate_node(self, mock_validate_node):
        node = {'type': 'Site'}
        validate([node])
        mock_validate_node.assert_called_once_with(node)


if __name__ == '__main__':
    unittest.main()
