import unittest
import json
from hestia_earth.schema import SiteSiteType

from .utils import fixtures_path
from hestia_earth.validation.validators.site import validate_site, validate_soilTexture, validate_depths, \
    validate_site_dates, validate_lifespan, validate_site_coordinates, validate_measurements_value


class TestValidatorsSite(unittest.TestCase):
    def test_validate_valid(self):
        with open(f"{fixtures_path}/site/valid.json") as f:
            node = json.load(f)
        self.assertListEqual(validate_site(node), [True] * 11)

    def test_validate_soilTexture_invalid(self):
        # 90% on same depthUpper / depthLower
        with open(f"{fixtures_path}/site/soilTexture/low-value.json") as f:
            site = json.load(f)
        self.assertEqual(validate_soilTexture(site['measurements']), {
            'level': 'error',
            'dataPath': '.measurements',
            'message': 'The sum of Sand, Silt, and Clay content should equal 100% for each soil depth interval.'
        })

        # remove all depthUpper / depthLower
        with open(f"{fixtures_path}/site/soilTexture/no-depth-high-value.json") as f:
            site = json.load(f)
        self.assertEqual(validate_soilTexture(site['measurements']), {
            'level': 'error',
            'dataPath': '.measurements',
            'message': 'The sum of Sand, Silt, and Clay content should equal 100% for each soil depth interval.'
        })

    def test_validate_soilTexture_valid(self):
        # missing element same depthUpper / depthLower
        with open(f"{fixtures_path}/site/soilTexture/missing-soil.json") as f:
            site = json.load(f)
        self.assertEqual(validate_soilTexture(site['measurements']), True)

        # simple no depth
        with open(f"{fixtures_path}/site/soilTexture/no-depth-valid.json") as f:
            site = json.load(f)
        self.assertEqual(validate_soilTexture(site['measurements']), True)

        # missing at least 1 value
        with open(f"{fixtures_path}/site/soilTexture/missing-values.json") as f:
            site = json.load(f)
        self.assertEqual(validate_soilTexture(site['measurements']), True)

    def test_validate_depths_valid(self):
        with open(f"{fixtures_path}/site/depths/valid.json") as f:
            site = json.load(f)
        self.assertEqual(validate_depths(site['measurements']), True)

    def test_validate_depths_invalid(self):
        with open(f"{fixtures_path}/site/depths/invalid.json") as f:
            site = json.load(f)
        self.assertEqual(validate_depths(site['measurements']), {
            'level': 'error',
            'dataPath': '.measurements[1].depthLower',
            'message': 'must be greater than depthUpper'
        })

    def test_validate_site_dates_valid(self):
        site = {
            'startDate': '2020-01-01',
            'endDate': '2020-01-02'
        }
        self.assertEqual(validate_site_dates(site), True)

    def test_validate_site_dates_invalid(self):
        site = {
            'startDate': '2020-01-02',
            'endDate': '2020-01-01'
        }
        self.assertEqual(validate_site_dates(site), {
            'level': 'error',
            'dataPath': '.endDate',
            'message': 'must be greater than startDate'
        })

    def test_validate_lifespan_valid(self):
        with open(f"{fixtures_path}/site/lifespan/valid.json") as f:
            site = json.load(f)
        self.assertEqual(validate_lifespan(site['infrastructure']), True)

    def test_validate_lifespan_invalid(self):
        with open(f"{fixtures_path}/site/lifespan/invalid.json") as f:
            site = json.load(f)
        self.assertEqual(validate_lifespan(site['infrastructure']), {
            'level': 'error',
            'dataPath': '.infrastructure[0].lifespan',
            'message': 'must equal to endDate - startDate in decimal years (~2.6)'
        })

    def test_need_validate_coordinates(self):
        site = {'siteType': SiteSiteType.CROPLAND.value}
        self.assertEqual(validate_site_coordinates(site), False)
        site['latitude'] = 0
        site['longitude'] = 0
        self.assertEqual(validate_site_coordinates(site), True)
        site['siteType'] = SiteSiteType.AQUACULTURE_PENS
        self.assertEqual(validate_site_coordinates(site), False)

    def test_validate_measurement_value_valid(self):
        with open(f"{fixtures_path}/site/measurements/value-valid.json") as f:
            site = json.load(f)
        self.assertEqual(validate_measurements_value(site.get('measurements')), True)

    def test_validate_measurement_value_invalid(self):
        with open(f"{fixtures_path}/site/measurements/value-above.json") as f:
            site = json.load(f)
        self.assertEqual(validate_measurements_value(site.get('measurements')), {
            'level': 'error',
            'dataPath': '.measurements[0].value',
            'message': 'should be below 12000'
        })

        with open(f"{fixtures_path}/site/measurements/value-below.json") as f:
            site = json.load(f)
        self.assertEqual(validate_measurements_value(site.get('measurements')), {
            'level': 'error',
            'dataPath': '.measurements[0].value',
            'message': 'should be above 0'
        })


if __name__ == '__main__':
    unittest.main()
