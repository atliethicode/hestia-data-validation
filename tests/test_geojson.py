import unittest

from hestia_earth.validation.geojson import get_geojson_area


class TestGeosjon(unittest.TestCase):
    def test_get_geojson_area(self):
        expected = 51120789339.6
        coordinates = [[[-180, -90], [-180, 90], [180, 90], [180, -90], [-180, -90]]]

        self.assertEqual(get_geojson_area({
            'type': 'Polygon',
            'coordinates': coordinates
        }), expected)

        self.assertEqual(get_geojson_area({
            'type': 'MultiPolygon',
            'coordinates': [coordinates]
        }), expected)

        self.assertEqual(get_geojson_area({
            'type': 'FeatureCollection',
            'features': [{
                'type': 'Feature',
                'geometry': {
                    'type': 'Polygon',
                    'coordinates': coordinates
                }
            }]
        }), expected)

        self.assertEqual(get_geojson_area({
            'type': 'GeometryCollection',
            'geometries': [{
                'type': 'Feature',
                'geometry': {
                    'type': 'Polygon',
                    'coordinates': coordinates
                }
            }]
        }), expected)


if __name__ == '__main__':
    unittest.main()
