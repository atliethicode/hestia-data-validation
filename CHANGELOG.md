# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.8](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.7...v0.1.8) (2021-01-05)


### Bug Fixes

* **site:** validate soil texture only if all values are present ([c90342c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c90342c9b0a81f2ea8381a5ca73b27885f2abf01)), closes [#29](https://gitlab.com/hestia-earth/hestia-data-validation/issues/29)

### [0.1.7](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.6...v0.1.7) (2021-01-04)


### Features

* **organisation:** validate `country`, `area` and coordinates ([4abbcef](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4abbcef26b48ac6c9300bbfe9bbd0f6e590b41ac))


### Bug Fixes

* **shared:** improve speed validate coordinates by region ([bdf4746](https://gitlab.com/hestia-earth/hestia-data-validation/commit/bdf47468b1eee8f172a96fd17a004e73a6d80b78))

### [0.1.6](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.5...v0.1.6) (2020-12-07)


### Features

* **site:** validate measurements value between min and max ([d7fc3f6](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d7fc3f61ead723b71bd0819e4c51144be9ad9c6e)), closes [#7](https://gitlab.com/hestia-earth/hestia-data-validation/issues/7)


### Bug Fixes

* **validators:** allow "region-" terms to be used as countries ([0a71c3d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0a71c3d59f42248ac6c0074a66572a7d78c22942))

### [0.1.5](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.4...v0.1.5) (2020-11-29)


### Features

* remove checks on `termType` ([4ed008f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4ed008f5eb3e50934384402cad27106c5acd3bdd))
* **validators:** add check `termType` = `system` to practice ([7311adf](https://gitlab.com/hestia-earth/hestia-data-validation/commit/7311adf6aef3f469714503eb3a9a3e01e5abb249))


### Bug Fixes

* **cycle:** handle duplicates in emissions.inputs.[@id](https://gitlab.com/id) ([19b88cd](https://gitlab.com/hestia-earth/hestia-data-validation/commit/19b88cdd4596ac06254e1ec75949816f7f2fa8b5))

### [0.1.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.3...v0.1.4) (2020-11-26)


### Features

* **cycle:** validate min/max on emissions, inputs, products and practices ([3240f83](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3240f83139af523ecb8aed12b3e366a11f93819a))
* **impact assessment:** add validators country, region and impacts ([12f29dc](https://gitlab.com/hestia-earth/hestia-data-validation/commit/12f29dc7973755b1db1ed40462bfd27acdf4f599))
* **impact assessment:** validate duplicates in impacts/emissionsResourceUse ([c2d36dc](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c2d36dcf3209e905a334569eb9b11296c720755a))
* **site:** validate min/max on measurements ([2d4e8b4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/2d4e8b44aab5528cf5c27f618bd43d0001789ba5))
* **term:** handle termType check on lists ([adbe8ff](https://gitlab.com/hestia-earth/hestia-data-validation/commit/adbe8ff5b503d378b63a818776431b91e0acdcb0))


### Bug Fixes

* **property-to-termType.csv:** update termType matching ([524d292](https://gitlab.com/hestia-earth/hestia-data-validation/commit/524d2927d858082959220e005ee29d15e456b438))

### [0.1.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.2...v0.1.3) (2020-11-13)


### Bug Fixes

* **package:** add missing file in manifest ([9c72929](https://gitlab.com/hestia-earth/hestia-data-validation/commit/9c7292960ec616a23809da7b406d12971da1816b))

### [0.1.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.1...v0.1.2) (2020-11-13)


### Bug Fixes

* **cycle:** check duplicated emissions including input.[@id](https://gitlab.com/id) ([bd508d0](https://gitlab.com/hestia-earth/hestia-data-validation/commit/bd508d0381f87e62570fea89869539162ac0b102))

### [0.1.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.0...v0.1.1) (2020-11-13)


### Features

* **validators:** add more termType validations ([f76fea5](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f76fea50b8ece11ee56c6be565ad5f8e3c79c8d0))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.14...v0.1.0) (2020-11-12)


### Features

* **cycle:** validate altStartDate not with startDate ([b08acab](https://gitlab.com/hestia-earth/hestia-data-validation/commit/b08acab90268ae779404b582809545062322e9cd))
* **cycle:** validate must have altStartDateDefinition with altStartDate ([d4a92df](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d4a92df4781a76de2411e4fe1f6b56f6e3c01f0e))
* **site:** add validator on country ([66f98db](https://gitlab.com/hestia-earth/hestia-data-validation/commit/66f98db992777c4c96d5cef8747ae32745e1479d)), closes [#16](https://gitlab.com/hestia-earth/hestia-data-validation/issues/16)


### Bug Fixes

* **lookups:** update termTypes from schema 0.2.0 ([a9be63a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a9be63a6703543c7e2669ecaec49f6f5fb7d5565))

### [0.0.14](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.13...v0.0.14) (2020-11-09)


### Features

* this is a drop-in replacement of version 0.0.13 ([c59d7c3](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c59d7c37d69caeecc6d050e7cad3af09a3d69a56))

### [0.0.13](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.12...v0.0.13) (2020-11-09)


### Bug Fixes

* add missing files in package ([92e2c42](https://gitlab.com/hestia-earth/hestia-data-validation/commit/92e2c4281e40da92b617f6b2ac13f450a8963c8a))

### [0.0.12](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.11...v0.0.12) (2020-11-09)


### Features

* **validators:** add missing country termType checks ([bba0475](https://gitlab.com/hestia-earth/hestia-data-validation/commit/bba04759088778446b3bfe033e42baf0ac580e58))
* **validators:** validate node children termType ([9de8d14](https://gitlab.com/hestia-earth/hestia-data-validation/commit/9de8d14c88124377fa5be7aaa2d586f0afc20bb2)), closes [#15](https://gitlab.com/hestia-earth/hestia-data-validation/issues/15)


### Bug Fixes

* **validators:** add missing Emission.input termType check ([e97058d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e97058db5fb9e3d56ffe9dd6bd6107814ac5fcf3)), closes [#15](https://gitlab.com/hestia-earth/hestia-data-validation/issues/15)
* **validators:** handle dataPath on subchildren error ([8306a41](https://gitlab.com/hestia-earth/hestia-data-validation/commit/8306a41a46a89f4efd291c7ece7b0129d6afaafd))
* **validators:** remove destination termType from Inventory.product ([ea0c58d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ea0c58d3877492ed2bc83b3a09c6343421f3d413)), closes [#15](https://gitlab.com/hestia-earth/hestia-data-validation/issues/15)

### [0.0.11](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.10...v0.0.11) (2020-11-06)


### Features

* **validators site:** get faster results check coordinates in country ([82b009a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/82b009a2886e73371c4fa94d45d16363f6b12e56))

### [0.0.10](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.9...v0.0.10) (2020-11-02)


### Bug Fixes

* **site:** update error message on region not in country ([a6c68f3](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a6c68f3d3b2805880f3c2b256b0614f1b0046c9c))

### [0.0.9](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.8...v0.0.9) (2020-09-17)


### Features

* **site:** skip validate coordinates for non-inland types ([15484b7](https://gitlab.com/hestia-earth/hestia-data-validation/commit/15484b7066dbb6af4604181dd5b28c731d521f96)), closes [#13](https://gitlab.com/hestia-earth/hestia-data-validation/issues/13)

### [0.0.8](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.7...v0.0.8) (2020-09-17)


### Bug Fixes

* **gadm:** fix parsing of level from id ([ed902e5](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ed902e5b27579b82604d301a456395e67308b5a4))

### [0.0.7](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.6...v0.0.7) (2020-09-16)


### Features

* **site:** test region is within country ([6291ed1](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6291ed1e0fdecc47cc5da92c88db88340c2219ed))
* **site:** validate coordinates in country/region ([2eb2977](https://gitlab.com/hestia-earth/hestia-data-validation/commit/2eb2977ab67979822c879021c4a65ead45a81fde)), closes [#4](https://gitlab.com/hestia-earth/hestia-data-validation/issues/4)
* **validation:** run each node in parallel ([e007eed](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e007eedc25b44c7759670a6536db39afff333a96))

### [0.0.6](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.5...v0.0.6) (2020-09-15)


### Bug Fixes

* allow equal dates when day is not specified ([81fe065](https://gitlab.com/hestia-earth/hestia-data-validation/commit/81fe065006ebd6da09f9030f06baae1e390121f5))

### [0.0.5](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.4...v0.0.5) (2020-09-05)


### Features

* **cycle:** check for duplicates in emissions, inputs and products ([68ba87a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/68ba87a9e6b8536a9886bd5f72d30538046c525d)), closes [#2](https://gitlab.com/hestia-earth/hestia-data-validation/issues/2)
* **cycle:** validate children relDays and value length ([d581a60](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d581a605cfa54d5ac67e4697e97d5ef1e5940c3f)), closes [#12](https://gitlab.com/hestia-earth/hestia-data-validation/issues/12)
* **site:** check area from boundary ([bf0dae4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/bf0dae4c070ad7f325a0909cc8ada07f90c43375)), closes [#11](https://gitlab.com/hestia-earth/hestia-data-validation/issues/11)
* **site:** check for duplicates in measurements ([b4ca753](https://gitlab.com/hestia-earth/hestia-data-validation/commit/b4ca7531c3ebb31c14a4162fb595252e23f9c54d))

### [0.0.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.3...v0.0.4) (2020-09-04)


### Features

* **cycle:** valid emissions dates ([caa3b9f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/caa3b9f79d22a67ffcaa3c97c1472a0de58a2d7a)), closes [#5](https://gitlab.com/hestia-earth/hestia-data-validation/issues/5)
* **cycle:** validate cycleDuration ([01952c0](https://gitlab.com/hestia-earth/hestia-data-validation/commit/01952c0c2ef4c26226ed278215874eb75da5c218)), closes [#9](https://gitlab.com/hestia-earth/hestia-data-validation/issues/9)
* **cycle:** validate endDate > startDate ([9538d61](https://gitlab.com/hestia-earth/hestia-data-validation/commit/9538d6111ca71276ab4758a1106785a0ca15e727))
* **cycle:** validate functionalUnitMeasure ([ed180f5](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ed180f5019115179cbf3735964a611b0dbf5c377)), closes [#10](https://gitlab.com/hestia-earth/hestia-data-validation/issues/10)
* **cycle:** validate practices endDate > startDate ([df8fe14](https://gitlab.com/hestia-earth/hestia-data-validation/commit/df8fe14dc3ec979690a2bef7dd8706798169cfb1)), closes [#5](https://gitlab.com/hestia-earth/hestia-data-validation/issues/5)
* **organisation:** validate endDate > startDate ([286e21e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/286e21eb83bf8e7c7cd6b66e2674afaef6bc23d2)), closes [#5](https://gitlab.com/hestia-earth/hestia-data-validation/issues/5)
* **site:** check lifespan for infrastructure ([0fa6e99](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0fa6e99549642fbb3c4e651602c5be5edee1eecd)), closes [#8](https://gitlab.com/hestia-earth/hestia-data-validation/issues/8)
* **site:** show calculated lifespan in error message ([d5097f3](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d5097f332ca22cd5dc34e8772a9e63b4de22dad9))
* **site:** valid measurements dates ([dc45c07](https://gitlab.com/hestia-earth/hestia-data-validation/commit/dc45c0709cb56ac929f34e703d4eb6c5ff8fcde3)), closes [#5](https://gitlab.com/hestia-earth/hestia-data-validation/issues/5)
* **site:** validate depthUpper > depthLower ([8dc5524](https://gitlab.com/hestia-earth/hestia-data-validation/commit/8dc5524064f5e61298949b85a2175ea9f821a557)), closes [#6](https://gitlab.com/hestia-earth/hestia-data-validation/issues/6)
* **site:** validate endDate > startDate ([0681986](https://gitlab.com/hestia-earth/hestia-data-validation/commit/06819864ce52edc8f031c92f11ed5b00cd1b7dda))
* **site:** validate infrastructure endDate > startDate ([a201535](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a201535f841b821356999962905f440dd5f414a5)), closes [#5](https://gitlab.com/hestia-earth/hestia-data-validation/issues/5)


### Bug Fixes

* **site:** fix check depthUpper < depthLower ([3d90848](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3d90848f096f409d901ebfad5bab6d9a1eea8492)), closes [#6](https://gitlab.com/hestia-earth/hestia-data-validation/issues/6)

### [0.0.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.2...v0.0.3) (2020-08-14)


### Features

* **validators:** handle recursive validation ([7e50335](https://gitlab.com/hestia-earth/hestia-data-validation/commit/7e5033511ed70d28def4bcae0b0ad4a9dd085292))


### Bug Fixes

* **site:** handle no 'measurements' key ([466ff48](https://gitlab.com/hestia-earth/hestia-data-validation/commit/466ff488f73d193f35bd878a8966de51aa7e4c91))
* **validators:** handle both type and [@type](https://gitlab.com/type) ([cc6601e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/cc6601e3cd096ef01c9adc7d75def28f856fc694))

### [0.0.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.1...v0.0.2) (2020-08-13)


### Bug Fixes

* **site:** handle type instead of [@type](https://gitlab.com/type) ([cb00db5](https://gitlab.com/hestia-earth/hestia-data-validation/commit/cb00db509aabb62e87eb9000285febd7e64ed27c))

### [0.0.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.0...v0.0.1) (2020-08-11)


### Features

* **site:** add site validation on measurements ([ad83051](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ad83051343367703df7852009c90b8f98bf8a517))
* **site:** set message level ([e6137ab](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e6137abd69fd2b5c6fd288d9f8df897e005c6f27))
* **validators:** add dummy site validation ([4a9d0f4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4a9d0f493ee4eece69e5b33d44110bf58a3405b6))


### Bug Fixes

* **site:** handle case multiple soilType values for same term ([45e2e55](https://gitlab.com/hestia-earth/hestia-data-validation/commit/45e2e558d332586ec67ddb718644de8b3854f2ae))
* **site:** handle measurements same depth fields ([f7556de](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f7556de5f4250357e3f91e8b682bb316ffdc2fe3))
